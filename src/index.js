import React from "react";
import ReactDOM from "react-dom";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import MessageList from "./components/message-list";
const NewApp = require("./components/message-list").default;

function renderApp() {
  ReactDOM.render(<App />, document.getElementById("root"));
}

const mainTheme = createMuiTheme({
  palette: {
    primary: {
      500: "#05E2C4"
    },
    error: {
      500: "#F56236"
    },
    warn: {
      500: "#FCE788"
    },
    info: {
      500: "#88FCA3"
    },
    black: {
      500: "#000000"
    }
  }
});

const App = () => (
  <MuiThemeProvider theme={mainTheme}>
    <MessageList />
  </MuiThemeProvider>
);

renderApp();

if (module.hot) {
  module.hot.accept("./components/message-list", () => {
    renderApp(NewApp);
  });
}
