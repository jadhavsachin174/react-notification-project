import React, { Component } from "react";
import Api from "../api";
import Header from "./header";
import ButtonContainer from "./button-container";
import Container from "./container";
import {
  ERROR_PRIORITY,
  WARN_PRIORITY,
  INFO_PRIORITY
} from "../utils/constants";

class MessageList extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      messages: [],
      showSnackbar: false,
      timer: null,
      errorCount: 0,
      warnCount: 0,
      infoCount: 0,
      isApiStarted: null
    };
  }

  api = new Api({
    messageCallback: message => {
      this.messageCallback(message);
    }
  });

  componentDidMount() {
    this.api.start();
  }

  componentWillUnmount() {
    clearTimeout(this.state.timer)
  }

  hideSnackBar = () => {
    clearTimeout(this.state.timer);
    this.setState({
      showSnackbar: false,
      timer: null
    });
  };

  messageCallback(message) {
    const { messages } = this.state;
    let tm = null;
    if (message.priority === ERROR_PRIORITY) {
      setTimeout(() => this.hideSnackBar(), 2000);
    }
    this.setState(prevState => ({
      messages: [message, ...messages.slice()],
      showSnackbar: message.priority === ERROR_PRIORITY,
      timer: message.priority === ERROR_PRIORITY ? tm : null,
      errorCount:
        message.priority === ERROR_PRIORITY
          ? prevState.errorCount + 1
          : prevState.errorCount,
      warnCount:
        message.priority === WARN_PRIORITY
          ? prevState.warnCount + 1
          : prevState.warnCount,
      infoCount:
        message.priority === INFO_PRIORITY
          ? prevState.infoCount + 1
          : prevState.infoCount
    }));
  }

  startStopClicked = () => {
    const isApiStarted = this.api.isStarted();

    if (isApiStarted) {
      this.api.stop();
    } else {
      this.api.start();
    }
    this.forceUpdate();
    this.setState({
      isApiStarted: isApiStarted
    });
  };

  clearClicked = () => {
    this.setState({
      messages: [],
      errorCount: 0,
      warnCount: 0,
      infoCount: 0,
      time: null
    });
  };

  clearMessage = index => {
    var array = [...this.state.messages];
    var element = array[index];
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ messages: array });

      this.setState(prevState => ({
        messages: array,
        errorCount:
          element.priority === ERROR_PRIORITY
            ? prevState.errorCount - 1
            : prevState.errorCount,
        warnCount:
          element.priority === WARN_PRIORITY
            ? prevState.warnCount - 1
            : prevState.warnCount,
        infoCount:
          element.priority === INFO_PRIORITY
            ? prevState.infoCount - 1
            : prevState.infoCount
      }));
    }
  };

  render() {
    return (
      <div>
        <Header
          showSnackbar={this.state.showSnackbar}
          hideSnackBar={this.hideSnackBar}
        />
        <ButtonContainer
          clearClicked={this.clearClicked}
          isApiStarted={this.state.isApiStarted}
          startStopClicked={this.startStopClicked}
        />
        <Container
          clearMessage={this.clearMessage}
          messages={this.state.messages}
          errorCount={this.state.errorCount}
          warnCount={this.state.warnCount}
          infoCount={this.state.infoCount}
        />
      </div>
    );
  }
}

export default MessageList;
