import React from "react";
import { line_color } from "../utils/colors";
import { HEADER_LABEL } from "../utils/constants";
import { createUseStyles } from "react-jss";
import { SnackbarContent, IconButton, Snackbar} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import CloseIcon from "@material-ui/icons/Close";

const useStyles = createUseStyles({
  headerContainer: {
    fontSize: 20,
    paddingTop: 15,
    paddingBottom: 15,
    marginBottom: 15,
    borderBottom: "2px solid",
    borderBottomColor: line_color,
    fontFamily: "sans-serif",
    paddingLeft: 25,
    justifyContent: "center"
  }
});

const styles = theme => ({
  error: {
    background: theme.palette.error[500]
  },
  icon: {
    fontSize: 20
  },
  message: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.black[500]
  }
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, ...other } = props;

  return (
    <SnackbarContent
      className={classes.error}
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          {message}
        </span>
      }
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.close}
          onClick={onClose}
        >
          <CloseIcon className={classes.icon} />
        </IconButton>
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent);

const Header = props => {
  const classes = useStyles();
  return (
    <div className={classes.headerContainer}>
      {HEADER_LABEL}
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        autoHideDuration={2000}
        open={props.showSnackbar}
        message="Error"
        onClose={() => props.hideSnackBar()}
      >
        <MySnackbarContentWrapper
          onClose={props.hideSnackBar}
          message="Error"
        />
      </Snackbar>
    </div>
  );
};

export default Header;
