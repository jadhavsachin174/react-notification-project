import React from "react";
import { Grid, SnackbarContent, Button } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import { createUseStyles } from "react-jss";
import { black } from "../utils/colors";
import { ERROR_PRIORITY, WARN_PRIORITY } from "../utils/constants";

const useStyles = createUseStyles({
  columnContainer: {
    margin: 10
  },
  typeLabel: {
    display: "block",
    fontFamily: "sans-serif",
    marginBottom: 5,
    color: black,
    fontSize: 20
  },
  countLabel: {
    fontSize: 12,
    fontFamily: "sans-serif",
    marginBottom: 10
  }
});

const details = [
  {
    key: 1,
    type: "Error"
  },
  {
    key: 2,
    type: "Warning"
  },
  {
    key: 3,
    type: "Info"
  }
];

const getCountForType = (props, type) => {
  return type === ERROR_PRIORITY
    ? props.errorCount
    : type === WARN_PRIORITY
    ? props.warnCount
    : props.infoCount;
};

const styles = theme => ({
  error: {
    background: theme.palette.error[500],
    marginBottom: 5,
    marginTop: 5
  },
  warn: {
    background: theme.palette.warn[500],
    marginBottom: 5,
    marginTop: 5
  },
  info: {
    background: theme.palette.info[500],
    marginBottom: 5,
    marginTop: 5
  },
  icon: {
    fontSize: 10,
    color: theme.palette.black[500]
  },
  message: {
    display: "flex",
    alignItems: "center",
    color: theme.palette.black[500]
  }
});

function MySnackbarContent(props) {
  const { classes, className, message, onClose, type, ...other } = props;

  return (
    <SnackbarContent
      className={
        type === ERROR_PRIORITY
          ? classes.error
          : type === WARN_PRIORITY
          ? classes.warn
          : classes.info
      }
      aria-describedby="client-snackbar"
      message={
        <span id="client-snackbar" className={classes.message}>
          {message}
        </span>
      }
      action={[
        <Button
          key="close"
          aria-label="Close"
          color="inherit"
          className={classes.icon}
          onClick={onClose}
        >
          Clear
        </Button>
      ]}
      {...other}
    />
  );
}

const MySnackbarContentWrapper = withStyles(styles)(MySnackbarContent);

const Container = props => {
  const classes = useStyles();
  return (
    <>
      <Grid container spacing={8}>
        <Grid item xs={12}>
          <Grid container justify="center" spacing={8}>
            {details.map((value, index) => (
              <Grid
                className={classes.columnContainer}
                key={`${index}${value}`}
                xs={3}
                item
              >
                <div className={classes.typeLabel}>
                  {`${value.type} Type ${value.key}`}
                </div>
                <div className={classes.countLabel}>
                  {`Count ${getCountForType(props, value.key)}`}
                </div>
                {props.messages.map((item, index) => {
                  if (item.priority === value.key) {
                    return (
                      <MySnackbarContentWrapper
                        key={`${index}${value}`}
                        onClose={() => props.clearMessage(index)}
                        message={item.message}
                        type={item.priority}
                      />
                    );
                  }
                })}
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Container;
