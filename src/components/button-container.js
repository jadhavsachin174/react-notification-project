import React from "react";
import Button from "@material-ui/core/Button";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  buttonContainer: {
    fontSize: 20,
    paddingTop: 5,
    paddingBottom: 15,
    marginBottom: 50,
    fontFamily: "sans-serif",
    display: "flex",
    justifyContent: "center"
  }
});


const ButtonContainer = props => {
  const classes = useStyles();
  return (
    <div className={classes.buttonContainer}>
      <Button
        variant="contained"
        color="primary"
        onClick={props.startStopClicked}
      >
        {props.isApiStarted ? "START" : "STOP"}
      </Button>&emsp;
      <Button
        variant="contained"
        color="primary"
        onClick={props.clearClicked}
      >
        CLEAR
      </Button>
    </div>
  );
};

export default ButtonContainer;
