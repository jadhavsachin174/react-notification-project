export const ERROR_PRIORITY = 1
export const WARN_PRIORITY = 2
export const INFO_PRIORITY = 3
export const HEADER_LABEL = "Help.com Coding Challenge"